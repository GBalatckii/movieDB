import 'jsdom-global/register';
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { ViewAllMovies } from '../code/ViewAllMovies';

configure({ adapter: new Adapter() });

describe('View all movies', () => {
  let props;
  let mountedViewAllMovies;

  // set default before each test
  beforeEach(() => {
    props = {
      filmData: {
        movies: [],
        error: '',
      },
      updateValues: () => null,
    };
    mountedViewAllMovies = undefined;
  });

  // function to get ViewAllMovies DOM
  const viewAllMovies = () => {
    if (!mountedViewAllMovies) {
      mountedViewAllMovies = shallow(<ViewAllMovies {...props} />);
    }
    return mountedViewAllMovies;
  };

  test('loaded correctly with header, button and table', () => {
    const header = viewAllMovies().find('h1');
    expect(header.text().trim()).toEqual('View all Movies:');

    const dropdownButton = viewAllMovies().find('button.dropdown__button');
    expect(dropdownButton.text().trim()).toEqual('Sort films');
    expect(dropdownButton).toHaveLength(1);

    const table = viewAllMovies().find('table.body__table');
    expect(table).toHaveLength(1);
    expect(table.find('tbody')).toHaveLength(1);

  });

  test('shows an error if no movies', () => {
    props.filmData.error = 'error';
    const films = viewAllMovies();
    expect(films.find('h3').text().trim()).toEqual('error');
    // Length: 2, tr with column headers and tr with an error
    expect(films.find('table.body__table > tbody > tr')).toHaveLength(2);
  });

  test('shows movies if no error', () => {
    props.filmData.movies = [
      {
        title: 'FilmTitel1',
        year: 1990,
        ratings: {
          critics_score: 49,
          audience_score: 51
        },
        abridged_cast: [
          { name: 'Actor1' },
          { name: 'Actor2' },
          { name: 'Actor3' },
          { name: 'Actor4' },
          { name: 'Actor5' },
        ],
        alternate_ids: {
          imdb: '1234567',
        },
      },
      {
        title: 'FilmTitel2',
        year: 1990,
        ratings: {
          critics_score: 51,
          audience_score: 49
        },
        abridged_cast: [
          { name: 'Actor1' },
          { name: 'Actor2' },
          { name: 'Actor3' },
          { name: 'Actor4' },
          { name: 'Actor5' },
        ],
        alternate_ids: {
          imdb: '1234567',
        },
      }
    ];
    const films = viewAllMovies();
    // Length: 5, tr with headers, with film1, with hidden actors1, with film2, with hidden actors2
    expect(films.find('table.body__table > tbody > tr')).toHaveLength(5);
  });

  test('dropdown hover opens the menu', () => {
    const films = viewAllMovies();
    const dropdownButton = films.find('button.dropdown__button');
    let dropList = films.find('div.dropdown__list');

    expect(films.state().showSort).toEqual(false);
    expect(dropList.hasClass('hide')).toEqual(true);

    dropdownButton.simulate('mouseover');
    expect(films.state().showSort).toEqual(true);
    dropList = films.find('div.dropdown__list');
    expect(dropList.hasClass('hide')).toEqual(false);
  });

  test('menu list click changes the sort', () => {
    const films = viewAllMovies();
    const buttons = films.find('div.dropdown__list > button');
    const matches = ['title', 'year', 'critics_score', 'audience_score'];

    buttons.forEach((val, index) => {
      val.simulate('click');
      expect(films.state().sort).toMatch(matches[index]);
    });
  });
});
