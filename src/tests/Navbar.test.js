import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Navbar } from '../code/Navbar';

// to work with react 16
configure({ adapter: new Adapter() });

describe('Navigations bar', () => {
  let mountedNavbar;

  beforeEach(() => {
    mountedNavbar = undefined;
  });

  const navBar = () => {
    if(!mountedNavbar) {
      mountedNavbar = shallow(<Navbar />);
    }
    return mountedNavbar;
  };

  test('loaded correctly', () => {
    const header = navBar().find('h3');
    expect(header.text()).toEqual('FILMS.de');
  });

  test('has HOME button', () => {
    const link = navBar().find('a[href="/"]');
    expect(link).toHaveLength(1);
    expect(link.find('i.fa-home')).toHaveLength(1);
  });

  test('has actor button', () => {
    const actors = navBar().find('a[href="#/actors"]');
    expect(actors).toHaveLength(1);
    expect(actors.text().trim()).toEqual('Actors');
  });

  test('has films dropdown', () => {
    const dropdownList = navBar().find('DropList');
    expect(dropdownList).toHaveLength(1);
  });
});