import React from 'react';
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jsdom-global/register';

import { Actors, ActorButton, FilmLink } from '../code/ActorList';

// to work with react 16
configure({ adapter: new Adapter() });

describe('actor list', () => {
  let props;
  let mountedActorList;

  // set default before each test
  beforeEach(() => {
    props = {
      filmData: {
        actors: ['one', 'two', 'three'],
        hisFilms: [],
        error: '',
      },
      updateActors: () => null,
      updateFilms: () => null,
    };
    mountedActorList = undefined;
  });

  // function to get TopMovies DOM
  const actorList = () => {
    if (!mountedActorList) {
      mountedActorList = mount(<Actors {...props} />);
    }
    return mountedActorList;
  };

  test('it loades correctly', () => {
    const header = actorList().find('h1');
    expect(header.text().trim()).toEqual('Show all actors:');
  });

});
