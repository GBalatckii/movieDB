import 'jsdom-global/register';
import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { TopMovies } from '../code/TopMovies';

// to work with react 16
configure({ adapter: new Adapter() });

describe('Top movies', () => {
  let props;
  let mountedTopMovies;

  // set default before each test
  beforeEach(() => {
    props = {
      filmData: {
        movies: [],
        error: '',
      },
      updateValues: () => null,
    };
    mountedTopMovies = undefined;
  });

  // function to get TopMovies DOM
  const topMovies = () => {
    if (!mountedTopMovies) {
      mountedTopMovies = shallow(<TopMovies {...props} />);
    }
    return mountedTopMovies;
  };

  test('loaded correctly with header', () => {
    const header = topMovies().find('h1');
    expect(header.text().trim()).toEqual('Best movies:');
  });

  test('shows an error if no movies', () => {
    props.filmData.error = 'error';
    const films = topMovies();
    expect(films.find('h4').text().trim()).toEqual('error');
    expect(films.find('TopMoviesItem')).toHaveLength(0);
  });

  test('shows movies if no error', () => {
    props.filmData.movies = [
      {
        title: 'The Shawshank Redemption',
        year: 1994,
        ratings: {
          critics_score: 91,
          audience_score: 98
        },
        synopsis: 'someTexthere',
        posters: {
          original: 'http://content9.flixster.com/movie/11/16/67/11166727_tmb.jpg'
        },
        abridged_cast: [
          { name: 'Robert Gunton' },
          { name: 'Tim Robbins' },
          { name: 'Morgan Freeman' },
          { name: 'Bob Gunton' },
          { name: 'William Sadler' }
        ],
        alternate_ids: {
          imdb: '0111161',
        },
      }];
    const films = topMovies();
    expect(films.find('TopMoviesItem')).toHaveLength(1);
    expect(films.find('h4')).toHaveLength(0);
  });

  test('buttons change the state', () => {
    const films = topMovies();
    let button = films.find('button#users-button');
    button.simulate('click');
    expect(films.state().sort).toMatch('audience_score');
    button = films.find('button#critics-button');
    button.simulate('click');
    expect(films.state().sort).toMatch('critics_score');
  });

});
