import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import TopMoviesItem from '../code/TopMoviesItem';

configure({ adapter: new Adapter() });

describe('Movie panel in TopMovies', () => {
  let props;
  let mountedMovie;

  // set default before each test
  beforeEach(() => {
    props = {
      first: false,
      link: '1234567',
      title: 'FilmTitle',
      year: 1990,
      poster: 'PosterLink',
      synopsis: 'Description',
      critRating: 49,
      usrRating: 51,
      actors: [
        { name: 'Actor1' },
        { name: 'Actor2' },
        { name: 'Actor3' },
        { name: 'Actor4' },
        { name: 'Actor5' },
      ],
    };
    mountedMovie = undefined;
  });

  // function to get TopMoviesItem DOM
  const topMoviesItem = () => {
    if (!mountedMovie) {
      mountedMovie = shallow(<TopMoviesItem {...props} />);
    }
    return mountedMovie;
  };

  test('loaded with props correctly', () => {
    const panel = topMoviesItem();
    let  elem = panel.find('h3#panel-title');
    expect(elem.text().trim()).toMatch('FilmTitle (1990)');

    elem = panel.find('img[src="PosterLink"]');
    expect(elem).toHaveLength(1);

    elem = panel.find('div.film-body__movie-synopsis > p');
    expect(elem.text().trim()).toEqual('Description');

    elem = panel.find('a[href="#/actors?name=Actor1"]');
    expect(elem).toHaveLength(1);
    expect(elem.text().trim()).toEqual('Actor1');

    elem = panel.find('span#critics-span');
    expect(elem).toHaveLength(1);
    expect(elem.text().trim()).toEqual('49');

    elem = panel.find('span#users-span');
    expect(elem).toHaveLength(1);
    expect(elem.text().trim()).toEqual('51');
  });

  test('first panel is opened', () => {
    props.first = true;
    const panel = topMoviesItem();
    expect(panel.state().hide).toEqual(false);
  });

  test('second panel is closed', () => {
    const panel = topMoviesItem();
    expect(panel.state().hide).toEqual(true);
  });

  test('header click closes panel', () => {
    const panel = topMoviesItem();
    const panelHeader = panel.find('div.panel__panel-header');

    panelHeader.simulate('click');
    expect(panel.state().hide).toEqual(false);
    expect(panel.find('div.panel-header__panel-body').hasClass('panel-header__panel-body--show')).toEqual(true);
    expect(panel.find('div.panel-header__panel-body').hasClass('hide')).toEqual(false);

    panelHeader.simulate('click');
    expect(panel.state().hide).toEqual(true);
    expect(panel.find('div.panel-header__panel-body').hasClass('panel-header__panel-body--show')).toEqual(false);
    expect(panel.find('div.panel-header__panel-body').hasClass('hide')).toEqual(true);
  });

  test('body click should not close panel', () => {
    const panel = topMoviesItem();
    const panelBody = panel.find('div.panel-header__panel-body');

    panelBody.simulate('click', { stopPropagation: () => undefined });
    expect(panel.state().hide).toEqual(true);
    expect(panel.find('div.panel-header__panel-body').hasClass('showPanel')).toEqual(false);
    expect(panel.find('div.panel-header__panel-body').hasClass('hide')).toEqual(true);

    panelBody.simulate('click', { stopPropagation: () => undefined });
    expect(panel.state().hide).toEqual(true);
    expect(panel.find('div.panel-header__panel-body').hasClass('showPanel')).toEqual(false);
    expect(panel.find('div.panel-header__panel-body').hasClass('hide')).toEqual(true);
  });
});
