/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React from 'react';
import { connect } from 'react-redux';
import * as filmValues from '../actions/filmActions';
import './ViewAllMovies.scss';

export class ViewAllMovies extends React.Component {
  constructor() {
    super();

    // sort for sort the list of films
    // hide as array to hide all actor bars
    // showSort to hide the sortMenu after clicked
    this.state = {
      sort: 'title',
      hide: [],
      showSort: false
    };

    this.sortTop = this.sortTop.bind(this);
    this.setNewLength = this.setNewLength.bind(this);
    this.changeSort = this.changeSort.bind(this);
  }

  componentWillMount() {
    this.props.updateValues('');
  }

  // call this to get new HIDE.length
  setNewLength(len) {
    if (this.state.hide.length === len) { return; }
    const newArr = [];
    for (let i = 0; i < len; i += 1) { newArr.push(true); }
    this.setState({ hide: newArr });
  }

  // after each sort close all rows
  changeSort(name) {
    const newArr = [];
    for (let i = 0; i < this.state.hide.length; i += 1) { newArr.push(true); }
    this.setState({ sort: name, hide: newArr, showSort: false });
  }

  // hide all actor rows, toggle the pressed one
  toggleAll(id) {
    const newArr = this.state.hide.map((val, index) => {
      if (index === id) {
        return !val;
      }
      return true;
    });

    this.setState({ hide: newArr });
  }

  // Sort films
  sortTop() {
    return (a, b) => {
      let objA;
      let objB;

      switch (this.state.sort) {
        case 'title':
          objA = b[this.state.sort];
          objB = a[this.state.sort];
          break;
        case 'year':
          objA = a[this.state.sort];
          objB = b[this.state.sort];
          break;
        case 'critics_score':
        case 'audience_score':
          objA = a.ratings[this.state.sort];
          objB = b.ratings[this.state.sort];
          break;
        default: break;
      }

      if (objA > objB) {
        return -1;
      } else if (objA < objB) {
        return 1;
      }
      return 0;
    };
  }

  render() {
    // Error - error message from redux-state
    // Films - the new sorted list of films (filter function to fix the bug)
    const [error] = [this.props.filmData.error];
    const films = this.props.filmData.movies.filter(() => true).sort(this.sortTop());

    let table = [];
    const url = 'http://www.imdb.com/title/tt';

    // first row - ID, NAME, YEAR, CRITIC_RATING, USER_RATING, ACTOR button
    // second row - hidden bar with actor list
    if (films.length > 0) {
      this.setNewLength(films.length);

      for (let i = 0; i < films.length; i += 1) {
        const film1 = (
          <tr key={i}>
            <td> {i + 1} </td>
            <td> <a href={url + films[i].alternate_ids.imdb} target="_blank"> {films[i].title} </a></td>
            <td> {films[i].year} </td>
            <td> {films[i].ratings.critics_score} </td>
            <td> {films[i].ratings.audience_score} </td>
            <td className={this.state.hide[i] ? '' : 'table__td-actor--show'}>
              <button onClick={() => this.toggleAll(i)} className="td-actor__button">
                <i className={this.state.hide[i] ? 'fa fa-chevron-down' : 'fa fa-chevron-up'} />
              </button>
            </td>
          </tr>
        );
        const film2 = (
          <tr key={i + films.length}>
            <td colSpan={6} className={this.state.hide[i] ? 'table__td-all-actors hide' : 'table__td-all-actors'}>
              <a href={`#/actors?name=${films[i].abridged_cast[0].name}`} target="_blank"> {films[i].abridged_cast[0].name} </a>
              <a href={`#/actors?name=${films[i].abridged_cast[1].name}`} target="_blank"> {films[i].abridged_cast[1].name} </a>
              <a href={`#/actors?name=${films[i].abridged_cast[2].name}`} target="_blank"> {films[i].abridged_cast[2].name} </a>
              <a href={`#/actors?name=${films[i].abridged_cast[3].name}`} target="_blank"> {films[i].abridged_cast[3].name} </a>
              <a href={`#/actors?name=${films[i].abridged_cast[4].name}`} target="_blank"> {films[i].abridged_cast[4].name} </a>
            </td>
          </tr>
        );
        table.push(film1);
        table.push(film2);
      }
    } else {
      table = <tr><td colSpan={6}><h3>{error}</h3></td></tr>;
    }

    return (
      <div className="view-all-movies-body">

        <div className="body__header">
          <h1> View all Movies: </h1>
          <div className="header__dropdown">
            <button className="btn button--custom dropdown__button" onMouseOver={() => this.setState({ showSort: true })}>
              Sort films
              &nbsp;
              <i className="fa fa-chevron-down" />
            </button>
            <div className={this.state.showSort === true ? 'dropdown__list' : 'dropdown__list hide'}>
              <button
                className="btn button--custom"
                onClick={() => this.changeSort('title')}
              >
                Name
              </button>
              <button
                className="btn button--custom"
                onClick={() => this.changeSort('year')}
              >
                Year
              </button>
              <button
                className="btn button--custom"
                onClick={() => this.changeSort('critics_score')}
              >
                Critics Rating
              </button>
              <button
                className="btn button--custom"
                onClick={() => this.changeSort('audience_score')}
              >
                Users Rating
              </button>
            </div>
          </div>
        </div>

        <table className="body__table">
          <colgroup>
            <col className="table__row--five" />
            <col />
            <col className="table__row--ten" />
            <col className="table__row--ten" />
            <col className="table__row--ten" />
            <col className="table__row--ten" />
          </colgroup>
          <tbody>
            <tr>
              <th> # </th>
              <th> Name </th>
              <th> Year </th>
              <th> Critics </th>
              <th> User </th>
              <th> Actors </th>
            </tr>
            {table}
          </tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filmData: state.films || {}
});

const mapDispatchToProps = dispatch => ({
  updateValues: () => dispatch(filmValues.getNewValues())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewAllMovies);
