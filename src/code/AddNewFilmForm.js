/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React from 'react';
import './AddNewFilmForm.scss';

class AddNewFilmForm extends React.Component {
  constructor() {
    super();

    // INPUT ?= OK
    this.state = {
      rightInput: false,
    };

    this.checkInput = this.checkInput.bind(this);
  }

  // check input
  checkInput(e) {
    const { id, value } = e.target;

    // if empty
    if (value.length === 0) {
      this.setState({ rightInput: false });
      return;
    }

    // Title: 0-50 letters
    // Description: 200+ letters
    // Year: 1800-this.year
    // Both ratings: 0-100
    // Film link: 1-9999999
    // Poster link: RegExp (see the right input in info alert in AddNewFilm.js)
    // Actors: filter the list and if length is five => send the list to parent
    switch (id) {
      case 'title': {
        this.setState({ rightInput: Boolean(value.length <= 50) });
        return;
      }
      case 'desc': {
        this.setState({ rightInput: Boolean(value.length >= 200) });
        return;
      }
      case 'year': {
        const now = (new Date()).getFullYear();
        this.setState({ rightInput: Boolean(value >= 1800 && value <= now) });
        return;
      }
      case 'crsc':
      case 'ussc': {
        this.setState({ rightInput: Boolean(value >= 0 && value <= 100) });
        return;
      }
      case 'flink': {
        this.setState({ rightInput: Boolean(value >= 1 && value <= 9999999) });
        return;
      }
      case 'plink': {
        const regExp = /^https:\/\/images-na\.ssl-images-amazon\.com\/images\/M\/MV5B(\w+)@+\._V1_U(\w|,|_)+0,182,268_AL_\.jpg$/;
        this.setState({ rightInput: regExp.test(value) });
        return;
      }
      case 'actors': {
        const actors = value
        // clear the string for wrong symbols
          .replace(/[^a-zA-Z\s,.\-_'0-9]+/g, '')
          // clear multiply spaces
          .replace(/\s\s+/g, ' ')
          // clear spaces near ","
          .replace(/ , |, | ,/g, ',')
          // clear "," at the beginning, end or multiplied
          .replace(/^,+|,{2,}|,+$/g, '')
          // trim and save to Array
          .trim()
          .split(',');

        if (actors.length === 5) {
          this.setState({ rightInput: true });
          this.props.sortedList(actors);
        } else {
          this.setState({ rightInput: false });
        }

        break;
      }
      default: break;
    }
  }

  render() {
    // Textarea for table or input
    const table = (this.props.id === 'desc');

    // Checkmark if rightInput is TRUE else cross
    // For table => Lift the icon up
    const iconClass = `fa fa-lg body__icon
      ${this.state.rightInput ? 'fa-check' : 'fa-times'}
      ${table ? 'body__icon--last' : ''}`;

    // Label names according id
    const labelName = {
      title: 'Title',
      year: 'Year',
      crsc: 'Critics score',
      ussc: 'Users score',
      flink: 'Film link',
      plink: 'Poster link',
      actors: 'Actors',
      desc: 'Description'
    };

    // onMouseOver event is only for testing with "SetTestInput". Delete.
    return (
      <div className="add-new-film-form-body">
        <label
          htmlFor={this.props.id}
          className={table ? 'body__label label--textarea' : 'body__label'}
        >
          {labelName[this.props.id]}
        </label>
        {!table ?
          <input
            type={this.props.type}
            className="body__input"
            placeholder={this.props.placeholder}
            required
            id={this.props.id}
            autoComplete="off"
            onChange={this.checkInput}
            onMouseOver={this.checkInput}
          /> :
          <textarea
            className="body__textarea"
            placeholder={this.props.placeholder}
            required
            id={this.props.id}
            autoComplete="off"
            onChange={this.checkInput}
            onMouseOver={this.checkInput}
          />
        }
        <i className={iconClass} />
      </div>
    );
  }
}

export default AddNewFilmForm;
