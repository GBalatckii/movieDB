import React from 'react';
import { connect } from 'react-redux';
import * as filmValues from '../actions/filmActions';
import './TopMovies.scss';
import TopMoviesItem from './TopMoviesItem';

export class TopMovies extends React.Component {
  constructor() {
    super();

    this.sortData = this.sortData.bind(this);
    this.sortTop = this.sortTop.bind(this);

    // default values: set sort to critics score
    this.state = {
      sort: 'critics_score'
    };
  }

  componentWillMount() {
    this.props.updateValues();
  }

  // Delete all films, that don't have description
  sortData() {
    return this.props.filmData.movies.filter(val => val.synopsis && val.posters && val.posters.original);
  }

  // Sort the film list with best ratings
  sortTop() {
    return (a, b) => {
      const objA = a.ratings[this.state.sort];
      const objB = b.ratings[this.state.sort];
      if (objA > objB) {
        return -1;
      } else if (objA < objB) {
        return 1;
      }
      return 0;
    };
  }

  render() {
    // Error - error message from redux-state
    // Films - the new sorted list of films
    const [error] = [this.props.filmData.error];
    const films = (this.sortData()).sort(this.sortTop());

    // Show Error or Films
    let table = [];
    if (films.length > 0) {
      for (let i = 0; i < films.length; i += 1) {
        const film = (
          <TopMoviesItem
            key={i}
            first={i === 0}
            link={films[i].alternate_ids.imdb}
            title={films[i].title}
            year={films[i].year}
            poster={films[i].posters.original}
            synopsis={films[i].synopsis}
            critRating={films[i].ratings.critics_score}
            usrRating={films[i].ratings.audience_score}
            actors={films[i].abridged_cast}
          />);
        table.push(film);
      }
    } else {
      table = <h4> {error} </h4>;
    }
    return (
      <div className="top-movies-body">
        <h1>Best movies:</h1>
        <button
          className="btn button--custom body__button--sort"
          onClick={() => this.setState({ sort: 'audience_score' })}
          id="users-button"
        >
          <span><i className="fa fa-lg fa-smile-o" /> </span>
        </button>
        <button
          className="btn button--custom body__button--sort"
          onClick={() => this.setState({ sort: 'critics_score' })}
          id="critics-button"
        >
          <span><i className="fa fa-lg fa-star" /> </span>
        </button>
        {table}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  filmData: state.films || {},
});

const mapDispatchToProps = dispatch => ({
  updateValues: () => dispatch(filmValues.getNewValues())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TopMovies);
