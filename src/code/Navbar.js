import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import './Navbar.scss';

class Navbar extends React.Component {
  render() {
    return (
      <div className="nav-bar-body">
        <div className="body__header">
          <h3>
            FILMS.de
          </h3>
        </div>
        <div className="navbar body__main">
          <div className="main__header">
            <a href="/">
              <i className="fa fa-home" />
            </a>
          </div>
          <div className="main__body">
            <DropList name="Films" link1="#/topfilms" link2="#/viewallmovies" link3="#/addnewfilm" menuId="1" />
            <a href="#/actors">Actors</a>
          </div>
        </div>
      </div>
    );
  }
}

export function DropList(props) {
  return (
    <DropdownButton className="body__dropdown--button" title={props.name} id={props.menuId}>
      <MenuItem href={props.link1}> The best </MenuItem>
      <MenuItem href={props.link2}> View all </MenuItem>
      <MenuItem href={props.link3}> Add new </MenuItem>
    </DropdownButton>
  );
}

export default Navbar;
