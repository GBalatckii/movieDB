/* eslint-disable jsx-a11y/click-events-have-key-events */
import React from 'react';
import './TopMoviesItem.scss';

class TopMoviesItem extends React.Component {
  constructor() {
    super();

    this.state = {
      hide: true,
    };
  }

  // show the first panel only
  componentWillMount() {
    if (this.props.first) { this.setState({ hide: false }); }
  }

  render() {
    const url = `http://www.imdb.com/title/tt${this.props.link}`;
    return (
      <div className="panel best-movie-body">

        {/* Panel Header with "Title (year). On click hide(show) the panel" */}
        <div className="panel__panel-header" onClick={() => this.setState({ hide: !this.state.hide })}>
          <h3 id="panel-title"> {this.props.title} ({this.props.year}) </h3>

          {/* Panel Body. Check if closed and add the classes */}
          <div
            className={
              this.state.hide ?
                'panel-header__panel-body hide' :
                'panel-header__panel-body panel-header__panel-body--show'
            }
            onClick={e => e.stopPropagation()}
          >

            {/* Poster, Synopsis, Actors, Rating */}
            <div className="panel-body__movie-poster">
              <a href={url} target="_blank">
                <img src={this.props.poster} alt="poster" className="img-responsive center-block" />
              </a>
            </div>
            <div className="panel-body__film-body">
              <div className="film-body__movie-synopsis">
                <p>
                  {this.props.synopsis}
                </p>
              </div>
              <div className="film-body__movie-actors">
                <h3>Actors:</h3>
                <ul>
                  <li><a href={`#/actors?name=${this.props.actors[0].name}`} target="_blank" >{this.props.actors[0].name}</a>, </li>
                  <li><a href={`#/actors?name=${this.props.actors[1].name}`} target="_blank" >{this.props.actors[1].name}</a>, </li>
                  <li><a href={`#/actors?name=${this.props.actors[2].name}`} target="_blank" >{this.props.actors[2].name}</a>, </li>
                  <li><a href={`#/actors?name=${this.props.actors[3].name}`} target="_blank" >{this.props.actors[3].name}</a>, </li>
                  <li><a href={`#/actors?name=${this.props.actors[4].name}`} target="_blank" >{this.props.actors[4].name}</a>, </li>
                  <li>...</li>
                </ul>
              </div>
              <div className="film-body__film-rating">
                <h3>Rating:</h3>
                <div>
                  <span>
                    <i className="fa fa-star" />
                    <span id="critics-span"> {this.props.critRating} </span>
                  </span>
                  <span>&nbsp;/&nbsp;</span>
                  <span>
                    <i className="fa fa-smile-o" />
                    <span id="users-span"> {this.props.usrRating} </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TopMoviesItem;
