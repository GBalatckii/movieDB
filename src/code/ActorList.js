import React from 'react';
import { connect } from 'react-redux';
import * as filmValues from '../actions/filmActions';
import './ActorList.scss';

class ActorList extends React.Component {
  constructor() {
    super();

    this.showFilms = this.showFilms.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.handleChange = this.handleChange.bind(this);

    // showFilms: show films panel for this actor
    // actorName: input value, on load take a value from the link (#/actors?name=XXX)
    // changeToFixed: change the film panel to fixed after scroll
    this.state = {
      showFilms: false,
      actorName: '',
      changeToFixed: false,
    };
  }

  componentWillMount() {
    // Listener to change films panel position to fixed
    window.addEventListener('scroll', this.handleScroll);

    // check if it is an actor name in URL
    const index = window.location.hash.indexOf('=');
    const name = index !== -1 ? window.location.hash.slice(index + 1) : '';
    this.setState({ actorName: name });

    // timeouts to disable requests spam
    this.filmTimer = null;

    // send new fetch to get the actor list
    this.props.updateActors();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    if (window.pageYOffset > 250) {
      this.setState({ changeToFixed: true });
    } else {
      this.setState({ changeToFixed: false });
    }
  }

  // load new actor list
  // set timeout for 500ms to disable request spam by fast typing
  handleChange(e) {
    this.setState({ showFilms: false, actorName: e.target.value });
  }

  // load all films from this actor and show the film panel
  // set timeout for 500ms to disable request spam by fast clicking
  showFilms(e) {
    clearTimeout(this.filmTimer);
    this.setState({ showFilms: false });
    this.props.updateFilms(e.target.innerText).then(() => {
      this.setState({ showFilms: true });
    });
  }

  render() {
    // the lists with actors and films from redux
    const actorList = (this.props.filmData.actors || []).filter(val => val.toLowerCase().includes(this.state.actorName.toLowerCase()));
    const filmList = this.props.filmData.hisFilms || [];

    // sort both alphabetically
    actorList.sort();
    filmList.sort((a, b) => a.title > b.title);

    // create lists for actors and films
    const listActors = [];
    const listFilms = [];

    // fill actor list with buttons or error
    actorList.forEach((val) => {
      listActors.push(<ActorButton key={val.id} name={val} click={this.showFilms} />);
    });
    if (listActors.length === 0) { listActors.push(<h3> {this.props.filmData.error} </h3>); }

    // fill film list with links or error
    filmList.forEach((val) => {
      const url = `http://www.imdb.com/title/tt${val.link}`;
      listFilms.push(<FilmLink key={val.id} name={val.title} link={url} />);
    });
    if (listFilms.length === 0) { listFilms.push(<h4> {this.props.filmData.error} </h4>); }

    // class for film panel to choose from NORMAL, HIDE, FIXED
    let inputClass = '';
    if (this.state.showFilms) {
      inputClass = this.state.changeToFixed ?
        'container body__list--films body__list--sticky' :
        'container body__list--films ';
    } else {
      inputClass = 'container body__list--films hide';
    }

    return (
      <div className="actor-list-body">
        <h1> Show all actors: </h1>
        <input
          type="text"
          placeholder="Enter an actor name"
          className="body__input"
          onChange={this.handleChange}
          value={this.state.actorName}
        />
        <div>
          <div className="container body__list--actors">
            {listActors}
          </div>
          <div className={inputClass}>
            {listFilms}
          </div>
        </div>
      </div>
    );
  }
}

export function ActorButton(props) {
  return (
    <button className="btn button--custom" onClick={props.click}> {props.name} </button>
  );
}

export function FilmLink(props) {
  return (
    <a href={props.link} target="_blank"> {props.name} </a>
  );
}

const mapStateToProps = state => ({
  filmData: state.films || {},
});

const mapDispatchToProps = dispatch => ({
  updateActors: () => dispatch(filmValues.loadActors()),
  updateFilms: name => dispatch(filmValues.loadFilms(name))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActorList);
