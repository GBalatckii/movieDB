import React from 'react';
import { connect } from 'react-redux';
import * as filmValues from '../actions/filmActions';
import FormInput from './AddNewFilmForm';
import './AddNewFilm.scss';

class AddNewFilm extends React.Component {
  constructor() {
    super();

    // alert - to choose which alert to show (INFO, SUCCESS, ERROR) and which text
    // btn - disable the button while waiting for response
    // actorList - to save the sorted actor list
    this.state = {
      alert: 'info',
      alertText: '',
      btn: true,
      actorList: [],
    };

    this.checkSubmit = this.checkSubmit.bind(this);
    this.setTestinput = this.setTestinput.bind(this);
    this.saveActorList = this.saveActorList.bind(this);
  }

  // some test data. If remove - remove the button "SetTestData" aswell
  // eslint-disable-next-line
  setTestinput() {
    const url = 'https://images-na.ssl-images-amazon.com/images/M/' +
      'MV5BY2Q2NzQ3ZDUtNWU5OC00Yjc0LThlYmEtNWM3NTFmM2JiY2VhXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg';

    const inputText = [
      'FilmTitle',
      1990,
      52,
      100,
      '1234567',
      url,
      'FirstOne LastOne, FirstTwo LastTwo,' +
      'FirstThree LastThree, FirstFour LastFour, FirstFive LastFive'
    ];

    const elements = document.querySelectorAll('input');
    elements.forEach((val, index) => {
      elements[index].value = inputText[index];
    });
    const textArea = document.querySelector('textarea');
    textArea.value = 'Some description here to get 200 characters for success: \n' +
      'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod ' +
      'tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. ' +
      'At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea tak';
  }

  // save sorted actor list from input check
  saveActorList(list) {
    if (list) {
      this.setState({
        actorList: list
      });
    }
  }

  // if all values are TRUE then try to connect the server.
  checkSubmit(e) {
    const checkIcons = document.querySelectorAll('.fa-check');
    const rightInput = Boolean(checkIcons.length === 8);
    if (rightInput) {
      // disable the button and hide the alert box
      this.setState({ btn: false, alert: 'hideAlert' });

      // define data and JSON it
      const data = {
        title: e.target[0].value,
        year: e.target[1].value,
        ratings: {
          critics_score: e.target[2].value,
          audience_score: e.target[3].value
        },
        synopsis: e.target[7].value,
        posters: {
          original: e.target[5].value
        },
        abridged_cast: [
          { name: this.state.actorList[0] },
          { name: this.state.actorList[1] },
          { name: this.state.actorList[2] },
          { name: this.state.actorList[3] },
          { name: this.state.actorList[4] }
        ],
        alternate_ids: {
          imdb: e.target[4].value
        }
      };

      const dataJSON = JSON.stringify(data);

      // update the film list
      this.props.updateValue(dataJSON).then(() => {
        if (this.props.filmData.film !== undefined) {
          this.setState({ alert: 'success' });
        } else {
          this.setState({ alert: 'error', alertText: this.props.filmData.error });
        }
        this.setState({ btn: true });
      });
    } else {
      this.setState({ alert: 'error', alertText: 'Please check your input' });
    }

    window.scrollTo(0, 0);
    // prevent to refresh the page
    e.preventDefault();
  }

  render() {
    // custom alerts
    const alertStyle = () => {
      switch (this.state.alert) {
        case 'info':
          return (
            <div className="alert body__alert alert--info">
              <strong>Info!</strong> <br />
              <p>
                1) Go to IMDB.com (<a href="http://www.imdb.com"> click </a>) and find your film there<br />
                2) Set the title, year, critics rating and user rating ( both 0-100 )<br />
                3) For the &quot;film link&quot; check the URL of your film and copy only film id after &quot;tt&quot;, e.g. 0111161<br />
                4) On your film page, right click on the poster and choose &quot;open image in new tab&quot; <br />
                5) Copy the URL to the poster link <br />
                6) Add five main stars, playing in this movie ( check for cases please )<br />
                7) Add a storyline of your film, which you can find on it&apos;s page <br />
                8) Click &quot;Submit&quot; button when you are done!
              </p>
            </div>
          );
        case 'error': return (
          <div className="alert body__alert alert--error">
            <strong>Error!</strong> <br />
            <p>
              {this.state.alertText}
            </p>
          </div>
        );
        case 'success': return (
          <div className="alert body__alert alert--success">
            <strong>Success!</strong> <br />
            <p>
              Film was successfully added to our collection! <br />
              Title: {this.props.filmData.film.title} <br />
              Year: {this.props.filmData.film.year}
            </p>
          </div>
        );
        default: return false;
      }
    };
    return (
      <div className="add-new-film-body">
        <h1> Add new film</h1>
        <h4> NOTE: All fields are compulsory fields and have to be completed </h4>

        {alertStyle()}

        <form onSubmit={this.checkSubmit}>

          <FormInput type="text" placeholder="Film title" id="title" />
          <FormInput type="number" placeholder="1800 - 2017" id="year" />
          <FormInput type="number" placeholder="0 - 100" id="crsc" />
          <FormInput type="number" placeholder="0 - 100" id="ussc" />
          <FormInput type="number" placeholder="XXXXXXX" id="flink" />
          <FormInput type="text" placeholder="https://images...jpg" id="plink" />
          <FormInput type="text" placeholder="FirstName LastName, ... ( 5 )" id="actors" sortedList={list => this.saveActorList(list)} />
          <FormInput placeholder="Description (min 50 characters)" id="desc" />
          <button
            className={
              this.state.btn ? 'btn button--custom body__button' :
              'btn button--custom body__button disabled'
            }
          >
            {this.state.btn ? 'Submit' : 'Wait ...'}
          </button>
          <br />
        </form>

        <button
          className="btn button--custom body__button"
          onClick={this.setTestinput}
          id="setInputBtn"
        >
          SetTestInput
        </button>

      </div>
    );
  }
}

const mapStateToProps = state => ({
  filmData: state.films || {},
});

const mapDispatchToProps = dispatch => ({
  updateValue: data => dispatch(filmValues.setNewValue(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddNewFilm);
