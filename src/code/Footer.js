import React from 'react';
import './Footer.scss';

class Footer extends React.Component {
  render() {
    return (
      <div className="footer-body" >
        <h3>
          © Georgii Balatckii
        </h3>
      </div>
    );
  }
}

export default Footer;
