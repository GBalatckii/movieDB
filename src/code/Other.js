import React from 'react';

class Other extends React.Component {
  render() {
    return (
      <div>
        <h1>Error 404: Not found</h1>
      </div>
    );
  }
}

export default Other;
