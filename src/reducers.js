import { combineReducers } from 'redux';

const initialState = {
  movies: [],
  error: 'Connecting to server ...',
  actors: [],
  hisFilms: [],
};

const films = (state = initialState, action) => {
  switch (action.type) {
    // GET: list of films
    case 'FILMS_UPDATED': return { ...state, movies: action.payload.movies };
    case 'FILMS_REJECTED': return { ...state, error: action.payload.error, movies: [] };
    // POST: add new film
    case 'NEWFILM_UPDATED': return { ...state, film: action.payload.film };
    case 'NEWFILM_REJECTED': return { ...state, error: action.payload.error, film: undefined };
    // GET: list of actors
    case 'ACTORS_UPDATED': return { ...state, actors: action.payload.actors, hisFilms: [] };
    case 'ACTORS_REJECTED': return { ...state, error: action.payload.error, actors: [] };
    // GET: list of films with one actor
    case 'ACTORFILMS_UPDATED': return { ...state, hisFilms: action.payload.hisFilms };
    case 'ACTORFILMS_REJECTED': return { ...state, error: action.payload.error, hisFilms: [] };
    // Default
    default: return { ...state };
  }
};

const reducers = combineReducers({
  films
});

export default reducers;
