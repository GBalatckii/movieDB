import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import './App.scss';
import Navbar from './code/Navbar';
import Footer from './code/Footer';
import TopMovies from './code/TopMovies';
import Other from './code/Other';
import ViewAllMovies from './code/ViewAllMovies';
import AddNewFilm from './code/AddNewFilm';
import ActorList from './code/ActorList';

class App extends React.Component {
  render() {
    return (
      <HashRouter>
        <div className="app-body">
          <Route path="/" component={Navbar} />
          <div className="body__main-cont">
            <Switch>
              <Route exact path="/" component={TopMovies} />
              <Route exact path="/topfilms" component={TopMovies} />
              <Route exact path="/viewallmovies" component={ViewAllMovies} />
              <Route exact path="/addnewfilm" component={AddNewFilm} />
              <Route exact path="/actors" component={ActorList} />
              <Route path="*" component={Other} />
            </Switch>
          </div>
          <Route path="/" component={Footer} />
        </div>
      </HashRouter>
    );
  }
}

export default App;
