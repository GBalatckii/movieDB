/**
 * GET list of films
 */
export function getFilmsPending() {
  return {
    type: 'FILMS_PENDING',
  };
}

export function getFilmsUpdated(movies) {
  return {
    type: 'FILMS_UPDATED',
    payload: {
      movies
    }
  };
}

export function getFilmsRejected(exception) {
  return {
    type: 'FILMS_REJECTED',
    payload: {
      error: exception.message,
    }
  };
}

/**
 * POST add new film
 */
export function setNewFilmPending() {
  return {
    type: 'NEWFILM_PENDING',
  };
}

export function setNewFilmUpdated(message) {
  return {
    type: 'NEWFILM_UPDATED',
    payload: {
      film: message
    }
  };
}

export function setNewFilmRejected(exception) {
  return {
    type: 'NEWFILM_REJECTED',
    payload: {
      error: exception.message,
    }
  };
}

/**
 * GET load actors list
 */
export function loadActorsPending() {
  return {
    type: 'ACTORS_PENDING',
  };
}

export function loadActorsUpdated(actors) {
  return {
    type: 'ACTORS_UPDATED',
    payload: {
      actors
    }
  };
}

export function loadActorsRejected(exception) {
  return {
    type: 'ACTORS_REJECTED',
    payload: {
      error: exception.message,
    }
  };
}

/**
 * GET load films with this actor
 */
export function loadActorFilmsPending() {
  return {
    type: 'ACTORFILMS_PENDING',
  };
}

export function loadActorFilmsUpdated(films) {
  return {
    type: 'ACTORFILMS_UPDATED',
    payload: {
      hisFilms: films,
    }
  };
}

export function loadActorFilmsRejected(exception) {
  return {
    type: 'ACTORFILMS_REJECTED',
    payload: {
      error: exception.message,
    }
  };
}

/**
 * GET request to server: get the list of films
 * Response: list of films or an error
 */
export function getNewValues() {
  return (dispatch) => {
    dispatch(getFilmsPending());
    return fetch('http://localhost:3001/loadfilms/', { method: 'GET' })
      .then(response =>
        new Promise((resolve, reject) => {
          if (response.status === 200) {
            resolve(response);
          } else {
            reject(new Error(response.statusText));
          }
        }))
      .then(response => response.json())
      .then(data => dispatch(getFilmsUpdated(data.movies)))
      .catch(exc => dispatch(getFilmsRejected(exc)));
  };
}

/**
 * POST request to server: try to add new film
 * Response: film that was added or an error
 * @param {Object} dataToSend - New film with all parameters
 */
export function setNewValue(dataToSend) {
  return (dispatch) => {
    dispatch(setNewFilmPending());
    return fetch('http://localhost:3001/addnewfilm', {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: dataToSend
    })
      .then((response) => {
        if (response.status !== 200) {
          return response
            .text()
            .then((text) => { throw new Error(text); });
        }
        return response
          .json()
          .then(data => dispatch(setNewFilmUpdated(data)));
      })
      .catch(exc => dispatch(setNewFilmRejected(exc)));
  };
}

/**
 * GET request to server: try to get list of actors
 * Response: actors list or an error
 */
export function loadActors() {
  return (dispatch) => {
    dispatch(loadActorsPending());
    return fetch('http://localhost:3001/loadactors/', { method: 'GET' })
      .then((response) => {
        if (response.status !== 200) {
          return response
            .text()
            .then((text) => { throw new Error(text); });
        }
        return response
          .json()
          .then(data => dispatch(loadActorsUpdated(data)));
      })
      .catch(exc => dispatch(loadActorsRejected(exc)));
  };
}

/**
 * GET request to server: try to get list of films with this actor
 * Response: films list or an error
 * @param {String} actorName - Actor name
 */
export function loadFilms(actorName) {
  return (dispatch) => {
    dispatch(loadActorFilmsPending());
    return fetch(`http://localhost:3001/loadactorfilms/${actorName}`, { method: 'GET' })
      .then((response) => {
        if (response.status !== 200) {
          return response
            .text()
            .then((text) => { throw new Error(text); });
        }
        return response
          .json()
          .then(data => dispatch(loadActorFilmsUpdated(data)));
      })
      .catch(exc => dispatch(loadActorFilmsRejected(exc)));
  };
}
